package com.example.masquesdenis_harrat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button bouton1 = (Button) findViewById(R.id.modifier1);
        bouton1.setOnClickListener((View.OnClickListener)this);


        Button bouton2 = (Button) findViewById(R.id.modifier2);
        bouton2.setOnClickListener((View.OnClickListener)this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.modifier1)
        {
            Intent intent = new Intent(this,secondActivity.class);
            startActivityForResult(intent,1);
        }
        if(v.getId() == R.id.modifier2)
        {
            Intent intent = new Intent(this,thirdActivity.class);
            startActivityForResult(intent,2);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if(requestCode == 1)
        {
            TextView nom = (TextView) findViewById(R.id.nom);
            String m  = data.getStringExtra(secondActivity.NOM);
            nom.setText(m);

            TextView prenom = (TextView) findViewById(R.id.prenom);
            String p  = data.getStringExtra(secondActivity.PRENOM);
            prenom.setText(p);

            TextView tel = (TextView) findViewById(R.id.telephone);
            String t  = data.getStringExtra(secondActivity.TEL);
            tel.setText(t);
        }
        if (requestCode == 2)
        {
            TextView num = (TextView) findViewById(R.id.numrue);
            String m  = data.getStringExtra(thirdActivity.NUM);
            num.setText(m);

            TextView nom = (TextView) findViewById(R.id.rue);
            String n  = data.getStringExtra(thirdActivity.RUE);
            nom.setText(n);

            TextView code = (TextView) findViewById(R.id.cp);
            String c  = data.getStringExtra(thirdActivity.CP);
            code.setText(c);

            TextView ville = (TextView) findViewById(R.id.ville);
            String v  = data.getStringExtra(thirdActivity.VILLE);
            ville.setText(v);

        }

    }
}
