package com.example.masquesdenis_harrat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class thirdActivity extends AppCompatActivity implements View.OnClickListener{

    static final String NUM= "fr.ulco.zharrat.num";
    static final String RUE = "fr.ulco.zharrat.rue";
    static final String CP = "fr.ulco.zharrat.cp";
    static final String VILLE = "fr.ulco.zharrat.ville";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        Button bouton1 = (Button) findViewById(R.id.valider);
        bouton1.setOnClickListener((View.OnClickListener)this);

        Button bouton2 = (Button) findViewById(R.id.annuler);
        bouton2.setOnClickListener((View.OnClickListener)this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.valider)
        {
            Intent intent = new Intent();

            EditText numrue = (EditText) findViewById(R.id.numrue);
            EditText rue = (EditText) findViewById(R.id.nomrue);
            EditText cp = (EditText) findViewById(R.id.cp);
            EditText ville = (EditText) findViewById(R.id.ville);


            intent.putExtra(NUM,numrue.getText().toString());
            intent.putExtra(RUE,rue.getText().toString());
            intent.putExtra(CP,cp.getText().toString());
            intent.putExtra(VILLE,ville.getText().toString());

            setResult(RESULT_OK,intent);
            finish();
        }
        if(v.getId() == R.id.annuler)
        {

        }

    }
}
