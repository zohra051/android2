package com.example.masquesdenis_harrat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class secondActivity extends AppCompatActivity implements View.OnClickListener{

    static final String NOM = "fr.ulco.zharrat.nom";
    static final String PRENOM = "fr.ulco.zharrat.prenom";
    static final String TEL = "fr.ulco.zharrat.tel";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Button bouton1 = (Button) findViewById(R.id.valider);
        bouton1.setOnClickListener((View.OnClickListener)this);

        Button bouton2 = (Button) findViewById(R.id.annuler);
        bouton2.setOnClickListener((View.OnClickListener)this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.valider)
        {
            Intent intent = new Intent();

            EditText nom = (EditText) findViewById(R.id.nom);
            EditText prenom = (EditText) findViewById(R.id.prenom);
            EditText tel = (EditText) findViewById(R.id.phone);


            intent.putExtra(NOM,nom.getText().toString());
            intent.putExtra(PRENOM,prenom.getText().toString());
            intent.putExtra(TEL,tel.getText().toString());

            setResult(RESULT_OK,intent);
            finish();
        }
        if(v.getId() == R.id.annuler)
        {

        }

    }
}
