package com.example.testintent01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    public final static String CHAMP = "fr.ulco.zharrat.appli.CHAMP";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button Envoyer = (Button) findViewById(R.id.envoyer);
        Envoyer.setOnClickListener((View.OnClickListener)this);


    }

    public void onClick(View view){

        EditText champ = (EditText) findViewById(R.id.text);

            Intent intention = new Intent(this,NewActivity.class);
            intention.putExtra(CHAMP, champ.getText().toString());

            startActivity(intention);


    }


}
