package com.example.testintent01;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.content.Intent;
import android.widget.TextView;


public class NewActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newactivity);


        Intent intention = getIntent();
        String m  = intention.getStringExtra(MainActivity.CHAMP);

        TextView txt = (TextView)  findViewById(R.id.newText);
        txt.setText(m);

    }


}
