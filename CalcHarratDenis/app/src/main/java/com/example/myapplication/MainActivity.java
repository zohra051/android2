package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import static android.os.Process.SIGNAL_KILL;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button raz = (Button) findViewById(R.id.raz);
        raz.setOnClickListener((View.OnClickListener)this);

        Button egal = (Button) findViewById(R.id.egal);
        egal.setOnClickListener((View.OnClickListener)this);

        Button quitter = (Button) findViewById(R.id.quitter);
        quitter.setOnClickListener((View.OnClickListener)this);

    }

    public void onClick(View view){

        EditText champ1 = (EditText) findViewById(R.id.champ1);
        EditText champ2 = (EditText) findViewById(R.id.champ2);


        RadioButton plus =(RadioButton) findViewById(R.id.Plus);
        RadioButton moins =(RadioButton) findViewById(R.id.Moins);
        RadioButton multipl =(RadioButton) findViewById(R.id.Multiplie);
        RadioButton divise =(RadioButton) findViewById(R.id.Divise);

        TextView resultat = (TextView) findViewById(R.id.resultat);



        if(view.getId()==R.id.raz){

            champ1.setText("");
            champ2.setText("");
            resultat.setText("Resultat");
        }

        if(view.getId()==R.id.egal){

            if(! TextUtils.isEmpty(champ1.getText()) && ! TextUtils.isEmpty(champ2.getText())){
                if(plus.isChecked()){
                    int result = Integer.parseInt(champ1.getText().toString())+Integer.parseInt(champ2.getText().toString());
                    resultat.setText(""+result);
                }

               if(moins.isChecked()){
                    int result = Integer.parseInt(champ1.getText().toString())-Integer.parseInt(champ2.getText().toString());
                    resultat.setText(""+result);

                }
                if(multipl.isChecked()){
                    int result = Integer.parseInt(champ1.getText().toString())*Integer.parseInt(champ2.getText().toString());
                    resultat.setText(""+result);

                }
                if(divise.isChecked()){

                    if( Integer.parseInt(champ2.getText().toString()) != 0) {
                        int result = Integer.parseInt(champ1.getText().toString()) / Integer.parseInt(champ2.getText().toString());
                        resultat.setText("" + result);
                    }
                    else{
                        resultat.setText("Error ! ");
                    }

                }
            }


        }
        if(view.getId()==R.id.quitter){
            finish();
        }


    }


}
