package com.example.tpimageharrat_denis;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileNotFoundException;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    static final int CODE = 1;
    private ImageView imageréel ;
    private Bitmap Real ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button charger = (Button) findViewById(R.id.charger);
        charger.setOnClickListener((View.OnClickListener)this);

        Button restaurer = (Button) findViewById(R.id.restaurer);
        restaurer.setOnClickListener((View.OnClickListener)this);

        registerForContextMenu(findViewById(R.id.image));

        imageréel = (ImageView) findViewById(R.id.image);
        Real = ((BitmapDrawable)imageréel.getDrawable()).getBitmap();

    }


    /* OPTIONS MENU */


    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options,menu);
        return true;
    }


    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        switch(id){
            case R.id.verticaly_menu:
                onClickVerticaly();
                break;
            case R.id.horizontaly_menu:
                onClickHorizontaly();
                break;
            case R.id.rotationD_menu:
                onClickRotateDroite();
                break;
            case R.id.rotationG_menu:
                onClickRotateGauche();
                break;


        }
        return true;
    }


    @Override
    public void onClick(View v){

        if(v.getId() == R.id.charger){
            Intent alpha = new Intent ();
            alpha.setAction(Intent.ACTION_GET_CONTENT);
            alpha.setType("image/*");
            startActivityForResult(alpha,CODE);
        }

        if(v.getId() == R.id.restaurer){
            onClickRestaurer();
        }
    }


    public void onClickRestaurer(){
        ImageView imageView = (ImageView) findViewById(R.id.image);
        imageView.setImageBitmap(Real);

    }
    public void onClickHorizontaly(){
            /*espace de l'image*/
            ImageView imageView = (ImageView) findViewById(R.id.image);
            /*image que j'ai réupéré*/
            Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();

            Bitmap image = Bitmap.createBitmap(bitmap.getWidth(),bitmap.getHeight(),bitmap.getConfig());
            for(int i=0; i< bitmap.getWidth();i++){
                for(int j=0; j< bitmap.getHeight();j++){
                    image.setPixel(i,bitmap.getHeight()-1-j,bitmap.getPixel(i,j));
                }
            }
            imageView.setImageBitmap(image);
    }

    public void onClickVerticaly(){
        /*espace de l'image*/
        ImageView imageView = (ImageView) findViewById(R.id.image);
        /*image que j'ai réupéré*/
        Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();

        Bitmap image = Bitmap.createBitmap(bitmap.getWidth(),bitmap.getHeight(),bitmap.getConfig());

        for(int i=0; i< bitmap.getWidth();i++){
            for(int j=0; j< bitmap.getHeight();j++){
                image.setPixel(bitmap.getWidth()-i-1,j,bitmap.getPixel(i,j));
            }
        }
        imageView.setImageBitmap(image);
    }

    public void onClickRotateDroite(){
        /*espace de l'image*/
        ImageView imageView = (ImageView) findViewById(R.id.image);
        /*image que j'ai réupéré*/
        Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();

        Bitmap image = Bitmap.createBitmap(bitmap.getHeight(),bitmap.getWidth(),bitmap.getConfig());

        for(int i=0; i< bitmap.getWidth();i++){
            for(int j=0; j< bitmap.getHeight();j++){
                image.setPixel(image.getWidth()-j-1,i,bitmap.getPixel(i,j));
            }
        }
        imageView.setImageBitmap(image);
    }


    public void onClickRotateGauche(){
        /*espace de l'image*/
        ImageView imageView = (ImageView) findViewById(R.id.image);
        /*image que j'ai réupéré*/
        Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();

        Bitmap image = Bitmap.createBitmap(bitmap.getHeight(),bitmap.getWidth(),bitmap.getConfig());

        for(int i=0; i< bitmap.getWidth();i++){
            for(int j=0; j< bitmap.getHeight();j++){
                image.setPixel(j,image.getHeight()-i-1,bitmap.getPixel(i,j));
            }
        }
        imageView.setImageBitmap(image);
    }

    /* CONTEXT MENU*/

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){

        super.onCreateContextMenu(menu,v,menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context,menu);

    }

    public boolean onContextItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.Inverser_menu:
                onClickInverseColor();
                return true;
            case R.id.Gris_menu:
                onClickGreyLevel();
                return true;
            case R.id.Gris_menu2:
                onClickGrey2();
                return true;
            case R.id.Gris_menu3:
                onClickGrey3();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }


    public void onClickInverseColor(){
        /*espace de l'image*/
        ImageView imageView = (ImageView) findViewById(R.id.image);
        /*image que j'ai réupéré*/
        Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();

        Bitmap image = Bitmap.createBitmap(bitmap.getWidth(),bitmap.getHeight(),bitmap.getConfig());

        for(int i=0; i< bitmap.getWidth();i++){
            for(int j=0; j< bitmap.getHeight();j++){
                int pix = bitmap.getPixel(i,j);
                int r = Color.red(pix);
                int g = Color.green(pix);
                int b = Color.blue(pix);
                image.setPixel(i,j,Color.argb(255,255-r,255-g,255-b));
            }
        }
        imageView.setImageBitmap(image);
    }

    public void onClickGreyLevel(){
        /*espace de l'image*/
        ImageView imageView = (ImageView) findViewById(R.id.image);
        /*image que j'ai réupéré*/
        Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();

        Bitmap image = Bitmap.createBitmap(bitmap.getWidth(),bitmap.getHeight(),bitmap.getConfig());

        for(int i=0; i< bitmap.getWidth();i++){
            for(int j=0; j< bitmap.getHeight();j++){
                int pix = bitmap.getPixel(i,j);
                int r = Color.red(pix);
                int g = Color.green(pix);
                int b = Color.blue(pix);
                int moyenne = (r+g+b)/3;
                image.setPixel(i,j,Color.argb(255,moyenne,moyenne,moyenne));
            }
        }
        imageView.setImageBitmap(image);


    }


    public void onClickGrey2(){
        /*espace de l'image*/
        ImageView imageView = (ImageView) findViewById(R.id.image);
        /*image que j'ai réupéré*/
        Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();

        Bitmap image = Bitmap.createBitmap(bitmap.getWidth(),bitmap.getHeight(),bitmap.getConfig());

        for(int i=0; i< bitmap.getWidth();i++){
            for(int j=0; j< bitmap.getHeight();j++){
                int pix = bitmap.getPixel(i,j);
                int r = Color.red(pix);
                int g = Color.green(pix);
                int b = Color.blue(pix);

                int grandRG = Math.max(r,g);
                int grandAutre = Math.max(r,b);
                int autre = Math.max(g,b);

                int GRAND=0;
                if( grandRG > grandAutre)
                    GRAND = grandRG;
                else
                    GRAND = grandAutre;

                if(GRAND > autre)
                    GRAND = GRAND;
                else
                    GRAND=autre;

                int calcul = ( GRAND + Math.min(Math.min(r,g),Math.min(g,b)) )/2 ;
                image.setPixel(i,j,Color.argb(255,calcul,calcul,calcul));
            }
        }
        imageView.setImageBitmap(image);
    }

    public void onClickGrey3(){
        /*espace de l'image*/
        ImageView imageView = (ImageView) findViewById(R.id.image);
        /*image que j'ai réupéré*/
        Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();

        Bitmap image = Bitmap.createBitmap(bitmap.getWidth(),bitmap.getHeight(),bitmap.getConfig());

        for(int i=0; i< bitmap.getWidth();i++){
            for(int j=0; j< bitmap.getHeight();j++){
                int pix = bitmap.getPixel(i,j);
                int r = Color.red(pix);
                int g = Color.green(pix);
                int b = Color.blue(pix);
                int calcul = (int) ((0.21f * r )+ (0.72f*g)+(0.07f*b));
                image.setPixel(i,j,Color.argb(255,calcul,calcul,calcul));
            }
        }
        imageView.setImageBitmap(image);
    }


    /* ***************************************************************************/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);


        try{



            /* CHANGER DES IMAGES */


        /*Préparer les option de chargement de l'image*/
        BitmapFactory.Options option = new BitmapFactory.Options();
        option.inMutable = true; /* l'image peut être modifié */
        Uri urimage = data.getData();
        /* chargement de l'image */

        Bitmap bm = BitmapFactory.decodeStream(getContentResolver().openInputStream(urimage),null, option);
        Real=bm;
        ImageView imageView = (ImageView) findViewById(R.id.image);
        imageView.setImageBitmap(bm);

        TextView txt = (TextView) findViewById(R.id.uri);
        txt.setText(urimage.toString());




        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }


}
