package com.example.testintent02;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    static final String CHAMP = "fr.ulco.zharrat.CHAMP";
    static final int CODE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button Modifier = (Button) findViewById(R.id.modifier);
        Modifier.setOnClickListener((View.OnClickListener)this);

        Button valider = (Button) findViewById(R.id.valider);
        valider.setOnClickListener((View.OnClickListener)this);

    }

    public void onClick(View view){

        EditText champ = (EditText) findViewById(R.id.text);


        if(view.getId() == R.id.modifier){

            Intent intention = new Intent(this,secondActivity.class);
            intention.putExtra(CHAMP, champ.getText().toString());
            startActivityForResult(intention,CODE);
        }
        if(view.getId() == R.id.valider){

            TextView txt = (TextView) findViewById(R.id.champModif);
            champ.setText(txt.getText().toString());
            //Toast.makeText(this,"test",Toast.LENGTH_SHORT).show();
        }



    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);

        TextView txt = (TextView) findViewById(R.id.champModif);
        String m  = data.getStringExtra(secondActivity.RES);
        txt.setText(m);


    }
}
