package com.example.testintent02;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.icu.text.CaseMap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class secondActivity extends AppCompatActivity implements View.OnClickListener{



    static final String RES = "fr.ulco.zharrat.RES";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);



        TextView txt = (TextView)  findViewById(R.id.Champ2);
        Button majuscule = (Button) findViewById(R.id.majuscule);
        majuscule.setOnClickListener((View.OnClickListener)this);

        Button inverser = (Button) findViewById(R.id.inverser);
        inverser.setOnClickListener((View.OnClickListener)this);

        Intent intention = getIntent();
        String m  = intention.getStringExtra(MainActivity.CHAMP);

        txt.setText(m);




    }

    public void onClick(View view) {

        Intent resultat = new Intent();

        if(view.getId() == R.id.majuscule){
            TextView txt = (TextView)  findViewById(R.id.Champ2);
            resultat.putExtra(RES, txt.getText().toString().toUpperCase());
            setResult(RESULT_OK,resultat);
            finish();

        }
        if(view.getId() == R.id.inverser){
            TextView txt = (TextView)  findViewById(R.id.Champ2);
            StringBuffer bf = new StringBuffer(txt.getText().toString()).reverse();
            resultat.putExtra(RES, bf.toString());
            setResult(RESULT_OK,resultat);
            finish();
        }
    }


}
