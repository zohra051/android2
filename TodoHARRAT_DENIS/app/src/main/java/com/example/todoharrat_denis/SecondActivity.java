package com.example.todoharrat_denis;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contente2_main);

        Button annuler = (Button) findViewById(R.id.annuler_btn);
        annuler.setOnClickListener((View.OnClickListener)this);

        Button valider = (Button) findViewById(R.id.valider_btn);
        valider.setOnClickListener((View.OnClickListener)this);



    }

    public void onClick(View v){

        if(v.getId() == R.id.annuler_btn){

            finish();
        }
        if(v.getId() == R.id.valider_btn){
            Intent result = new Intent();
            TextView txt = findViewById(R.id.newtext);
            result.putExtra("newText",txt.getText().toString());
            setResult(RESULT_OK,result);
            finish();
        }
    }


}