package com.example.todoharrat_denis;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.SparseBooleanArray;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    ArrayList<String> liste = new ArrayList<>();
    ArrayAdapter<String> adapter;
ListView lv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener((View.OnClickListener)this);

        /*Lecture de fichier*/
        File path = this.getFilesDir();
        File file = new File(path, "data.txt");
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                liste.add(line);
            }
            br.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

/************************/

        lv = (ListView)findViewById(R.id.maListe);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_checked,liste);
        lv.setAdapter(adapter);


    }






    @Override
    public void onClick(View v){

        if(v.getId() == R.id.fab){
          //  Intent intent = new Intent(this,SecondActivity.class);
          //  startActivityForResult(intent,1);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
            final EditText input = new EditText(MainActivity.this);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            input.setLayoutParams(lp);

            builder
                    .setTitle("To do")
                    .setView(input)
                    .setIcon(R.drawable.todo)
                    .setPositiveButton("Valider",new DialogInterface.OnClickListener(){
                        public void onClick(DialogInterface dialog, int which){
                            //String task = String.valueOf(input.getText());
                            if(input != null && !input.getText().toString().equals("")) {
                                liste.add(input.getText().toString());
                                adapter.notifyDataSetChanged();
                            }
                        }
                      })
                    .setNegativeButton("Annuler",null)
                    .show();
        }

    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode, Intent data)
    {super.onActivityResult(requestCode,resultCode,data);
        /*
      if(data != null && !data.getStringExtra("newText").equals("")) {
          super.onActivityResult(requestCode,resultCode,data);
          String txt = data.getStringExtra("newText");
          liste.add(txt);
          adapter.notifyDataSetChanged();
        }
*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.Effacer:
                Effacer();
                return true;
            case R.id.Sauver:
                SauverListe();

            default:
                return false;
        }
    }


public void Effacer(){
    SparseBooleanArray b  = lv.getCheckedItemPositions();
    for(int i=lv.getAdapter().getCount()-1;i>=0;i--)
    {
        if(b.get(i))
        {
            liste.remove(i);
        }
    }
    b.clear();

    adapter.notifyDataSetChanged();
}


public void SauverListe(){
    File path = this.getFilesDir();
    File file = new File(path,"data.txt");
    String str = "\n";
    try {
        FileOutputStream stream = new FileOutputStream(file);
        for(int i=0;i<liste.size();i++)
        {
            System.out.println(liste.get(i));
            stream.write(liste.get(i).getBytes());
            stream.write(str.getBytes());
        }
        stream.close();
    }
    catch (Exception e)
    {
        e.printStackTrace();
    }
}













    /*

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.effacer) {
            ListView lv = (ListView)findViewById(R.id.maListe);
            SparseBooleanArray checked = lv.getCheckedItemPositions();
            for (int i = 0; i < lv.getAdapter().getCount(); i++) {
                if (checked.get(i)) {
                    liste.remove(i);
                }
            }
            checked.clear();
            adapter.notifyDataSetChanged();
            return true;
        }

        if(id == R.id.sauvegarder) {
            System.out.println("ok");
            File path = this.getFilesDir();
            File file = new File(path,"sauvegarde.txt");
            try {
                FileOutputStream stream = new FileOutputStream(file);
                for(int i=0;i<liste.size();i++)
                {
                    System.out.println(liste.get(i));
                    stream.write(liste.get(i).getBytes());
                }
                stream.close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    */
}
